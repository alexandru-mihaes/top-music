#include <stdio.h>
#include <sqlite3.h>

static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i;
    for(i = 0; i < argc; i++)
    {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}

void executeCmd(sqlite3 *db, char *selectMsg, char *table, char *message);

int main(int argc, char *argv[])
{
    sqlite3 *db;
    int rc;

    // OPEN
    /* Open database */
    rc = sqlite3_open("music.db", &db);

    if(rc)
    {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        return (0);
    } else
    {
        fprintf(stdout, "Opened database successfully\n");
    }

    // CREATE TABLES
    executeCmd(db, (char *) "CREATE TABLE USERS(" \
                            "USERNAME TEXT PRIMARY KEY NOT NULL," \
                            "PASSWORD TEXT             NOT NULL," \
                            "ADMIN    TEXT              NOT NULL," \
                            "TO_VOTE  TEXT              NOT NULL);",
               (char *) "users", (char *) "Table created successfully");
    executeCmd(db, (char *) "CREATE TABLE SONGS(" \
                            "ID          INT PRIMARY KEY NOT NULL," \
                            "TITLE       TEXT            NOT NULL," \
                            "ARTIST      TEXT            NOT NULL," \
                            "LINK        TEXT            NOT NULL," \
                            "DESCRIPTION TEXT            NOT NULL," \
                            "VOTES       INT             NOT NULL)",
               (char *) "songs", (char *) "Table created successfully");
    executeCmd(db, (char *) "CREATE TABLE VOTES(" \
                            "USERNAME TEXT NOT NULL," \
                            "ID_SONG  INT  NOT NULL)",
               (char *) "votes", (char *) "Table created successfully");
    executeCmd(db, (char *) "CREATE TABLE GENRES(" \
                            "ID_SONG INT  NOT NULL," \
                            "GENRE   TEXT NOT NULL)",
               (char *) "genres", (char *) "Table created successfully");


    // INSERT RECORDS
    executeCmd(db, (char *) "INSERT INTO USERS (USERNAME,PASSWORD,ADMIN,TO_VOTE)" \
                            "VALUES ('admin', '123456', 'YES', 'YES');" \
                            "INSERT INTO USERS (USERNAME,PASSWORD,ADMIN,TO_VOTE)" \
                            "VALUES ('alex', '654321', 'NO', 'YES');" \
                            "INSERT INTO USERS (USERNAME,PASSWORD,ADMIN,TO_VOTE)" \
                            "VALUES ('sarah', 'music', 'NO', 'YES');" \
                            "INSERT INTO USERS (USERNAME,PASSWORD,ADMIN,TO_VOTE)" \
                            "VALUES ('john90', 'topmusic', 'NO', 'YES');" \
                            "INSERT INTO USERS (USERNAME,PASSWORD,ADMIN,TO_VOTE)" \
                            "VALUES ('laura.popescu', 'Romania', 'NO', 'YES');" \
                            "INSERT INTO USERS (USERNAME,PASSWORD,ADMIN,TO_VOTE)" \
                            "VALUES ('adrian_ionescu', 'parola', 'NO', 'NO');",
               (char *) "users", (char *) "Records created successfully");

    executeCmd(db, (char *) "INSERT INTO SONGS (ID,TITLE,ARTIST,LINK,DESCRIPTION,VOTES)" \
                            "VALUES (1, 'Good Together', 'SHY Martin'," \
                            "'https://www.youtube.com/watch?v=Wa1AhNA5OJ8'," \
                            "'15 Dec 2017', 2);" \

                            "INSERT INTO SONGS (ID,TITLE,ARTIST,LINK,DESCRIPTION,VOTES)" \
                            "VALUES (2, 'Broken Summer (feat. Max Frost)', 'DJ Snake'," \
                            "'https://www.youtube.com/watch?v=614qPzyblQU'," \
                            "'23 Oct 2017', 3);" \

                            "INSERT INTO SONGS (ID,TITLE,ARTIST,LINK,DESCRIPTION,VOTES)" \
                            "VALUES (3, 'Stay Young (feat. Tessa)', 'Mike Perry'," \
                            "'https://www.youtube.com/watch?v=qZIDQc5Ikvc'," \
                            "'1 Sep 2017', 1);" \

                            "INSERT INTO SONGS (ID,TITLE,ARTIST,LINK,DESCRIPTION,VOTES)" \
                            "VALUES (4, 'Nirvana', 'INNA'," \
                            "'https://www.youtube.com/watch?v=OfS1jFck8YQ'," \
                            "'11 Dec 2017', 7);" \

                            "INSERT INTO SONGS (ID,TITLE,ARTIST,LINK,DESCRIPTION,VOTES)" \
                            "VALUES (5, 'Comets (feat. Natalia Doco)', 'Freddy Verano'," \
                            "'https://www.youtube.com/watch?v=J86GHa8bup8'," \
                            "'19 May 2015', 3);" \

                            "INSERT INTO SONGS (ID,TITLE,ARTIST,LINK,DESCRIPTION,VOTES)" \
                            "VALUES (6, 'Hurt By You', 'Emily Warren'," \
                            "'https://www.youtube.com/watch?v=fXHBDSTqH5Q'," \
                            "'15 Apr 2016', 9);",
               (char *) "songs", (char *) "Records created successfully");
    executeCmd(db, (char *) "INSERT INTO VOTES (USERNAME,ID_SONG)" \
                            "VALUES ('alex', 4);" \
                            "INSERT INTO VOTES (USERNAME,ID_SONG)" \
                            "VALUES ('alex', 6);",
               (char *) "votes", (char *) "Records created successfully");
    executeCmd(db, (char *) "INSERT INTO GENRES (ID_SONG,GENRE)" \
                            "VALUES (4, 'Club');" \
                            "INSERT INTO GENRES (ID_SONG,GENRE)" \
                            "VALUES (5, 'Dance');" \
                            "INSERT INTO GENRES (ID_SONG,GENRE)" \
                            "VALUES (4, 'Latin');" \
                            "INSERT INTO GENRES (ID_SONG,GENRE)" \
                            "VALUES (4, 'Pop');" \
                            "INSERT INTO GENRES (ID_SONG,GENRE)" \
                            "VALUES (2, 'Pop');" \
                            "INSERT INTO GENRES (ID_SONG,GENRE)" \
                            "VALUES (6, 'Pop');",
               (char *) "genres", (char *) "Records created successfully");

    // SELECT
    /*
    executeCmd(db, (char *) "SELECT * from USERS",
               (char *) "users", (char *) "Select with success");
    executeCmd(db, (char *) "SELECT * from SONGS",
               (char *) "songs", (char *) "Select with success");
    executeCmd(db, (char *) "SELECT * from GENRES",
               (char *) "genres", (char *) "Select with success");
    */

    sqlite3_close(db);
    return 0;
}

void executeCmd(sqlite3 *db, char *selectMsg, char *table, char *message)
{
    char *zErrMsg = 0;
    /* Create SQL statement */
    char *sql = selectMsg;

    /* Execute SQL statement */
    int rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "(%s) SQL error: %s\n", table, zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "(%s) %s\n", table, message);
    }
}