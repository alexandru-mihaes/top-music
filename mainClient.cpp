#include "Client/Client.h"

int main(int argc, char *argv[])
{
    // verificam daca exista toate argumentele la linia de comanda
    /*
    if(argc != 3)
    {
        printf("Sintaxa: %s <adresa_server> <port>\n", argv[0]);
        return -1;
    }
    */
    try
    {
        //Client *client = new Client(argv[1], argv[2]);
        char ip[10] = "127.0.0.1";
        char port[5] = "2908";
        Client *client = new Client(ip, port);

        if(client->startMenu())
            client->signIn();
        else
            client->signUp();
    }
    catch(char const *string)
    {
        perror(string);
    }

}