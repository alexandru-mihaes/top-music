# TopMusic

## Packages required for building

`g++`

`sqlite3`

`libsqlite3-dev`


## Build and run

The following commands must be executed (while in the `top-music` directory):

`cd cmake-build-debug`

`cmake ..`

`make`

`./createDatabase.out`

`./server.out`

`./client.out`