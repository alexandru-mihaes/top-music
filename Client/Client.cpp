#include "Client.h"

struct
{
    int id;
    char title[100];
    char artist[100];
    char link[100];
    char description[100];
    int votes;
    char genres[10][100];
    int nrGenres;
} songsDB[200];
int nrSongsDB;

struct
{
    char title[100];
    char artist[100];
    char link[100];
    char description[100];
} song;

Client::Client(char *ip, char *port)
{
    this->port = atoi(port); // stabilim portul
    createSocket(AF_INET, SOCK_STREAM, 0);
    establishIP(ip);
    connect2Server();
}

void Client::createSocket(int domain, int type, int protocol)
{
    if((sockfd = socket(domain, type, protocol)) == -1)
        throw "[client] Error: socket().\n";
}

void Client::establishIP(char *ip)
{
    // umplem structura folosita pentru realizarea conexiunii cu serverul
    server.sin_family = AF_INET;            // familia socket-ului
    server.sin_addr.s_addr = inet_addr(ip); // adresa IP a serverului
    server.sin_port = htons(port);          // portul de conectare
}

void Client::connect2Server()
{
    if(connect(sockfd, (struct sockaddr *) &server, sizeof(struct sockaddr)) == -1)
        throw "[client] Error: connect().\n";
}

// FUNCTII PENTRU COMUNICARE
void Client::getMsg()
{
    // citirea raspunsului dat de server (apel blocant pina cand serverul raspunde)
    bzero(msgReceived, 100);
    if(read(this->sockfd, msgReceived, sizeof(msgReceived)) < 0)
        throw "[client] Error: read() from server.\n";
}

void Client::printReceived()
{
    // afisam mesajul primit
    printf("%s", msgReceived);
}

void Client::sendMsg()
{
    // trimiterea mesajului la server
    if(write(this->sockfd, msgSent, sizeof(msgSent)) <= 0)
        throw "[client] Error: write() to server.\n";
}

void Client::printSent()
{
    printf("[client] The message sent is: %s\n", msgReceived);
}

void Client::keyboardRead()
{
    // citirea mesajului
    bzero(this->msgSent, 100);
    fflush(stdout);
    read(0, this->msgSent, sizeof(this->msgSent));
    msgSent[strlen(msgSent) - 1] = '\0';
}

void Client::setSent(char msg[])
{
    strcpy(msgSent, msg);
}

// PENTRU USER
void Client::clearScreen()
{
    printf("\033[2J\033[1;1H");
}

void Client::printHeader()
{
    printf("┌───────────────────────────────────────┐\n" \
           "│ TopMusic                              │\n" \
           "├───────────────────────────────────────┤\n");
}

bool Client::startMenu()
{
    bool incorrect = false;
    while(1)
    {
        clearScreen();
        printHeader();
        printf("│ Type a number according to your need: │\n" \
               "│ 1 (Sign In)              2 (Sign Up)  │\n" \
               "├───────────────────────────────────────┤\n" \
               "│                                       │\n");
        if(incorrect)
            printf("├───────────────────────────────────────┤\n" \
                   "│ You can only type 1 or 2.             │\n");
        printf("└───────────────────────────────────────┘\n");
        printf("\033[7;3H");
        keyboardRead();
        sendMsg();
        getMsg();
        if(strcmp("signIn", msgReceived) == 0)
            return true;
        else if(strcmp("signUp", msgReceived) == 0)
            return false;
        else incorrect = true;
    }
}

void Client::signIn()
{
    bool incorrect = false, signedIn = false;
    do
    {
        clearScreen();
        printHeader();
        printf("│ Username:                             │\n" \
               "│ Password:                             │\n");
        if(incorrect)
            printf("├───────────────────────────────────────┤\n" \
                   "│ Incorrect username or password.       │\n");
        printf("└───────────────────────────────────────┘\n");
        printf("\033[4;13H");
        keyboardRead();
        sendMsg();

        printf("\033[5;13H");
        keyboardRead();
        sendMsg();
        getMsg();
        if(strcmp(msgReceived, "signedIn") == 0)
        {
            startTopMusic();
            signedIn = true;
        } else
            incorrect = true;
    } while(!signedIn);
}

void Client::signUp()
{
    bool incorrect = false, usernameUsed = false, signedUp = false;
    do
    {
        clearScreen();
        printHeader();
        printf("│ Username:                             │\n" \
               "│ Password:                             │\n");
        if(incorrect)
            printf("├───────────────────────────────────────┤\n" \
                   "│ These are the valid characters for:   │\n" \
                   "│ - username: {a-zA-Z0-9._}             │\n" \
                   "│ - password: {a-zA-Z0-9._!#()-,~@}     │\n");
        if(usernameUsed)
            printf("├───────────────────────────────────────┤\n" \
                   "│ Username is already in use            │\n");
        printf("└───────────────────────────────────────┘\n");
        printf("\033[4;13H");
        keyboardRead();
        sendMsg();

        printf("\033[5;13H");
        keyboardRead();
        sendMsg();
        getMsg();
        if(strcmp(msgReceived, "signedUp") == 0)
        {
            startTopMusic();
            signedUp = true;
        } else if(strcmp(msgReceived, "usernameUsed") == 0)
        {
            usernameUsed = true;
            incorrect = false;
        } else
        {
            incorrect = true;
            usernameUsed = false;
        }
    } while(!signedUp);
}

void Client::startTopMusic()
{
    while(1)
    {
        clearScreen();
        getMsg();
        nrSongsDB = atoi(msgReceived);
        printf("┌───────────────────────────────────────┐\n" \
               "│ TopMusic                              │\n" \
               "├───────────────────────────────────────┘\n");
        printTop(nrSongsDB);

        printf("\n├───────────────────────────────────────┐\n" \
                 "│ These are the commands you can use:   │\n" \
                 "│ - add a song                          │\n" \
                 "│ - add a genre to a song               │\n" \
                 "│ - vote a song                         │\n" \
                 "│ - filter by genre                     │\n");
        getMsg();
        if(strcmp(msgReceived, "Admin") == 0)
            printf("│ - delete a song                       │\n" \
                   "│ - make a user admin                   │\n" \
                   "│ - block a user from voting            │\n");
        printf("│ - sign out                            │\n" \
               "│ - close app                           │\n" \
               "├───────────────────────────────────────┘\n");

        printf("│ > ");
        keyboardRead();
        sendMsg();
        if(strcmp(msgSent, "add a song") == 0)
            addSong();
        else if(strcmp(msgSent, "add a genre to a song") == 0)
            addGenreToSong();
        else if(strcmp(msgSent, "vote a song") == 0)
        {
            getMsg();
            if(strcmp(msgReceived, "You are allowed vote") == 0)
                voteSong();
            else
            {
                printf("├───────────────────────────────────────┐\n" \
                       "│ You are not allowed to vote anymore.  │\n" \
                       "├───────────────────────────────────────┘\n"
                               "│ Press enter to continue... ");
                std::cin.get();
            }
        } else if(strcmp(msgSent, "filter by genre") == 0)
            filterByGenre(nrSongsDB);
        else if(strcmp(msgSent, "delete a song") == 0)
        {
            getMsg();
            if(strcmp(msgReceived, "Admin") == 0)
                deleteSong(nrSongsDB);
            else
            {
                printf("├───────────────────────────────────────┐\n" \
                       "│ You are not allowed to delete a song. │\n" \
                       "├───────────────────────────────────────┘\n"
                               "│ Press enter to continue... ");
                std::cin.get();
            }
        } else if(strcmp(msgSent, "make a user admin") == 0)
        {
            getMsg();
            if(strcmp(msgReceived, "Admin") == 0)
                makeAdmin();
            else
            {
                printf("├───────────────────────────────────────┐\n" \
                       "│ You are not allowed to make a user    │\n" \
                       "│ admin.                                │\n" \
                       "├───────────────────────────────────────┘\n"
                               "│ Press enter to continue... ");
                std::cin.get();
            }

        } else if(strcmp(msgSent, "block a user from voting") == 0)
        {
            getMsg();
            if(strcmp(msgReceived, "Admin") == 0)
                blockUserVote();
            else
            {
                printf("├───────────────────────────────────────┐\n" \
                       "│ You are not able to make this.        │\n" \
                       "├───────────────────────────────────────┘\n"
                               "│ Press enter to continue... ");
                std::cin.get();
            }
        } else if(strcmp(msgSent, "close app") == 0)
        {
            exit();
            break;
        } else if(strcmp(msgSent, "sign out") == 0)
        {
            if(startMenu())
                signIn();
            else
                signUp();
            break;
        }
    }
}

void Client::printTop(int nrSongsDB)
{
    for(int i = 0; i < nrSongsDB; i++)
    {
        getMsg();
        songsDB[i].id = atoi(msgReceived);

        getMsg();
        strcpy(songsDB[i].title, msgReceived);

        getMsg();
        strcpy(songsDB[i].artist, msgReceived);

        getMsg();
        strcpy(songsDB[i].link, msgReceived);

        getMsg();
        strcpy(songsDB[i].description, msgReceived);

        getMsg();
        songsDB[i].votes = atoi(msgReceived);

        getMsg();
        songsDB[i].nrGenres = atoi(msgReceived);

        for(int g = 0; g < songsDB[i].nrGenres; g++)
        {
            getMsg();
            strcpy(songsDB[i].genres[g], msgReceived);
        }
    }
    for(int i = 0; i < nrSongsDB; i++)
    {
        printf("│ %d. %s\n", i + 1, songsDB[i].title);
        printf("│    %s\n", songsDB[i].artist);
        printf("│    Link: %s\n", songsDB[i].link);
        printf("│    Votes: %d\n", songsDB[i].votes);
        printf("│    Genres: ");
        for(int g = 0; g < songsDB[i].nrGenres; g++)
            printf("%s ", songsDB[i].genres[g]);
        if(i != nrSongsDB - 1)
            printf("\n├\n");
    }
}

void Client::addSong()
{
    clearScreen();
    printf("┌───────────────────────────────────────┐\n" \
           "│ TopMusic                              │\n" \
           "├───────────────────────────────────────┘\n" \
           "│ Title:\n" \
           "│ Artist:\n" \
           "│ Link:\n" \
           "│ Description:");

    printf("\033[4;10H");
    keyboardRead();
    strcpy(song.title, msgSent);
    printf("\033[5;11H");
    keyboardRead();
    strcpy(song.artist, msgSent);
    printf("\033[6;9H");
    keyboardRead();
    strcpy(song.link, msgSent);
    printf("\033[7;16H");
    keyboardRead();
    strcpy(song.description, msgSent);

    setSent(song.title);
    sendMsg();

    setSent(song.artist);
    sendMsg();

    setSent(song.link);
    sendMsg();

    setSent(song.description);
    sendMsg();

    bool invalid = false;
    getMsg();
    if(strcmp(msgReceived, "added") != 0)
        invalid = true;
    if(invalid)
    {
        printf("├───────────────────────────────────────┐\n" \
               "│ These are the valid characters:       │\n" \
               "│ {a-zA-Z0-9.!#()-,/?= }                │\n" \
               "├───────────────────────────────────────┘\n"
                       "│ Press enter to continue... ");
        std::cin.get();
    }
}

void Client::voteSong()
{
    clearScreen();
    printHeader();
    printf("│ Enter the rank number of the song you │\n" \
           "│ would like to vote.                   │\n" \
           "├───────────────────────────────────────┘\n");
    printf("│ > ");
    keyboardRead();
    int rank = atoi(msgSent);
    while(rank - 1 >= nrSongsDB || rank <= 0)
    {
        printf("│ > ");
        keyboardRead();
        rank = atoi(msgSent);
    }
    printf("│ Are you sure you want to vote\n│ this song?");
    printf("│ %s\n", songsDB[rank - 1].title);
    printf("│ %s\n", songsDB[rank - 1].artist);

    bool answer = false;
    do
    {
        printf("│ (YES/NO)\n");
        printf("│ > ");
        keyboardRead();
        if(strcmp(msgSent, "YES") == 0)
            answer = true;
        if(strcmp(msgSent, "NO") == 0)
            answer = true;
    } while(!answer);
    sendMsg();

    bool incorrect = false;
    if(strcmp(msgSent, "YES") == 0)
    {
        sprintf(msgSent, "%d", rank - 1);
        sendMsg();
        getMsg();
        if(strcmp(msgReceived, "acceptedVote") != 0)
            incorrect = true;
    }
    if(incorrect)
    {
        printf("├───────────────────────────────────────┐\n" \
               "│ You voted this song already.          │\n" \
               "├───────────────────────────────────────┘\n"
                       "│ Press enter to continue... ");
        std::cin.get();
    }
}

void Client::addGenreToSong()
{
    clearScreen();
    printHeader();
    printf("│ Enter the rank number of the song you │\n" \
           "│ would like to add a genre to.         │\n" \
           "├───────────────────────────────────────┘\n");
    printf("│ > ");
    keyboardRead();
    int rank = atoi(msgSent);
    while(rank - 1 >= nrSongsDB || rank <= 0)
    {
        printf("│ > ");
        keyboardRead();
        rank = atoi(msgSent);
    }
    printf("│ Are you sure you want to add a genre to \n│ this song?\n");
    printf("│ %s\n", songsDB[rank - 1].title);
    printf("│ %s\n", songsDB[rank - 1].artist);

    bool answer = false;
    do
    {
        printf("│ (YES/NO)\n");
        printf("│ > ");
        keyboardRead();
        if(strcmp(msgSent, "YES") == 0)
            answer = true;
        if(strcmp(msgSent, "NO") == 0)
            answer = true;
    } while(!answer);
    sendMsg();
    if(strcmp(msgSent, "YES") == 0)
    {
        sprintf(msgSent, "%d", rank - 1);
        sendMsg();

        printf("├───────────────────────────────────────┐\n" \
               "│ Enter the genre you would like to add │\n" \
               "│ to the song.                          │\n" \
               "├───────────────────────────────────────┘\n");
        printf("│ > ");
        keyboardRead();
        sendMsg();

        bool already = false, invalid = false;
        getMsg();
        if(strcmp(msgReceived, "invalidText") == 0)
            invalid = true;
        else if(strcmp(msgReceived, "Already has this genre") == 0)
            already = true;

        if(already)
        {
            printf("├───────────────────────────────────────┐\n" \
                   "│ This song already has this genre.     │\n" \
                   "├───────────────────────────────────────┘\n"
                           "│ Press enter to continue... ");
            std::cin.get();
        } else if(invalid)
        {
            printf("├───────────────────────────────────────┐\n" \
                   "│ These are the valid characters:       │\n" \
                   "│ - password: {a-zA-Z0-9.!#()-,/?= }    │\n" \
                   "├───────────────────────────────────────┘\n"
                           "│ Press enter to continue... ");
            std::cin.get();
        }
    }
}

void Client::filterByGenre(int nrSongsDB)
{
    clearScreen();
    printHeader();
    printf("│ Enter the genre you would like to     │\n" \
           "│ filter by.                            │\n" \
           "├───────────────────────────────────────┘\n");
    printf("│ > ");
    keyboardRead();
    if(!genreAvailable(msgSent))
    {
        printf("├───────────────────────────────────────┐\n" \
               "│ This genre is not available.          │\n" \
               "├───────────────────────────────────────┘\n"
                       "│ Press enter to continue... ");
        std::cin.get();
    } else
        printTopByGenre(nrSongsDB, msgSent);
}

bool Client::genreAvailable(char *msg)
{
    for(int i = 0; i < nrSongsDB; i++)
        for(int g = 0; g < songsDB[i].nrGenres; g++)
        {
            //printf("\ncomparing %s with %s", msg, songsDB[i].genres[g]);
            if(strcmp(msg, songsDB[i].genres[g]) == 0)
                return true;
        }
    return false;
}

void Client::printTopByGenre(int nrSongsDB, char *genre)
{
    clearScreen();
    printf("┌───────────────────────────────────────┐\n" \
           "│ TopMusic                              │\n" \
           "├───────────────────────────────────────┘\n");
    int genreRank = 1;
    for(int i = 0; i < nrSongsDB; i++)
    {
        bool matchGenre = false;
        for(int g = 0; g < songsDB[i].nrGenres; g++)
            if(strcmp(genre, songsDB[i].genres[g]) == 0)
                matchGenre = true;
        if(matchGenre)
        {
            printf("│ %d. %s\n", genreRank, songsDB[i].title);
            printf("│    %s\n", songsDB[i].artist);
            printf("│    Link: %s\n", songsDB[i].link);
            printf("│    Votes: %d\n", songsDB[i].votes);
            printf("│    Genres: ");
            for(int g = 0; g < songsDB[i].nrGenres; g++)
                printf("%s ", songsDB[i].genres[g]);
            printf("\n├\n");
            genreRank++;
        }

    }
    printf("│ Press enter to return... ");
    std::cin.get();
}

void Client::deleteSong(int nrSongsDB)
{
    clearScreen();
    printHeader();
    printf("│ Enter the rank number of the song you │\n" \
           "│ you would like to delete.             │\n" \
           "├───────────────────────────────────────┘\n");
    printf("│ > ");
    keyboardRead();
    int rank = atoi(msgSent);
    while(rank - 1 >= nrSongsDB || rank <= 0)
    {
        printf("│ > ");
        keyboardRead();
        rank = atoi(msgSent);
    }

    printf("│ Are you sure you want to delete\n│ this song?\n");
    printf("│ %s\n", songsDB[rank - 1].title);
    printf("│ %s\n", songsDB[rank - 1].artist);

    bool answer = false;
    do
    {
        printf("│ (YES/NO)\n");
        printf("│ > ");
        keyboardRead();
        if(strcmp(msgSent, "YES") == 0)
            answer = true;
        if(strcmp(msgSent, "NO") == 0)
            answer = true;
    } while(!answer);
    sendMsg();

    if(strcmp(msgSent, "YES") == 0)
    {
        sprintf(msgSent, "%d", rank - 1);
        sendMsg();
    }

}

void Client::makeAdmin()
{
    clearScreen();
    printHeader();

    getMsg();
    int nrUsers = atoi(msgReceived);
    printUsers(nrUsers);


    printf("├───────────────────────────────────────┐\n" \
           "│ Enter the the username you would like │\n" \
           "│ to make admin.                        │\n" \
           "├───────────────────────────────────────┘\n");
    printf("│ > ");

    keyboardRead();
    sendMsg();
    getMsg();
    if(strcmp(msgReceived, "Username does not exist") == 0)
    {
        printf("├───────────────────────────────────────┐\n" \
               "│ Username does not exist.              │\n" \
               "├───────────────────────────────────────┘\n"
                       "│ Press enter to continue... ");
        std::cin.get();
    } else
    {
        printf("├───────────────────────────────────────┐\n" \
           "│ Are you user you want to make this    │\n" \
           "│ user admin?                           │\n" \
           "├───────────────────────────────────────┘\n");

        bool answer = false;
        do
        {
            printf("│ (YES/NO)\n");
            printf("│ > ");
            keyboardRead();
            if(strcmp(msgSent, "YES") == 0)
                answer = true;
            if(strcmp(msgSent, "NO") == 0)
                answer = true;
        } while(!answer);
        sendMsg();
    }

}

void Client::printUsers(int nrUsers)
{
    for(int i = 0; i < nrUsers; i++)
    {
        getMsg();
        printf("│ %s\n", msgReceived);
    }
}

void Client::blockUserVote()
{
    clearScreen();
    printHeader();

    getMsg();
    int nrUsers = atoi(msgReceived);
    printUsers(nrUsers);


    printf("├───────────────────────────────────────┐\n" \
           "│ Enter the the username you would like │\n" \
           "│ to block from voting.                 │\n" \
           "├───────────────────────────────────────┘\n");
    printf("│ > ");

    keyboardRead();
    sendMsg();
    getMsg();
    if(strcmp(msgReceived, "Username does not exist") == 0)
    {
        printf("├───────────────────────────────────────┐\n" \
               "│ Username does not exist.              │\n" \
               "├───────────────────────────────────────┘\n"
                       "│ Press enter to continue... ");
        std::cin.get();
    } else
    {
        printf("├───────────────────────────────────────┐\n" \
           "│ Are you user you want to make this    │\n" \
           "│ user admin?                           │\n" \
           "├───────────────────────────────────────┘\n");

        bool answer = false;
        do
        {
            printf("│ (YES/NO)\n");
            printf("│ > ");
            keyboardRead();
            if(strcmp(msgSent, "YES") == 0)
                answer = true;
            if(strcmp(msgSent, "NO") == 0)
                answer = true;
        } while(!answer);
        sendMsg();
    }

}

void Client::exit()
{
    close(sockfd);
}