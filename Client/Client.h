#ifndef TOPMUSIC_CLIENT_H
#define TOPMUSIC_CLIENT_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cerrno>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <netdb.h>
#include <arpa/inet.h>
#include <cstring>
#include <iostream>

class Client
{
private:
    int port;
    struct sockaddr_in server; // structura folosita pentru conectare la server
    int sockfd;      // descriptorul de socket

    void createSocket(int domain, int type, int protocol);
    void establishIP(char *ip);
    void connect2Server();

    char msgReceived[100], msgSent[100]; // mesajul primit de la server si cel de trimis inapoi la server

    void getMsg();
    void printReceived();
    void sendMsg();
    void printSent();
    void keyboardRead();
    void setSent(char msg[]);
public:
    Client(char *ip, char *port);
    void clearScreen();
    void printHeader();
    bool startMenu();
    void signIn();
    void signUp();
    void startTopMusic();
    void printTop(int nrSongsDB);
    void addSong();
    void voteSong();
    void addGenreToSong();
    void filterByGenre(int nrSongsDB);
    bool genreAvailable(char *msg);
    void printTopByGenre(int nrSongsDB, char *genre);
    void deleteSong(int nrSongsDB);
    void makeAdmin();
    void printUsers(int nrUsers);
    void blockUserVote();
    void exit();
};

#endif //TOPMUSIC_CLIENT_H
