#include "Menu.h"

// PENTRU COMUNICARE
char msgReceived[100], msgSent[100]; // mesajul primit de la client si cel de trimis inapoi la client
int receivedLen = 0;

// PENTRU BAZA DE DATE
// variabile pentru select-uri
sqlite3 *db;
char *zErrMsg = 0;
int rc;
char *sql;

// variabile pentru baza de date
struct
{
    char username[100];
    char password[100];
    char admin[5];
    char to_vote[5];
} usersDB[200];
int nrUsersDB = 0;

struct
{
    int id;
    char title[100];
    char artist[100];
    char link[100];
    char description[100];
    int votes;
    char genres[10][100];
    int nrGenres = 0;
} songsDB[200];
int nrSongsDB = 0;

struct
{
    char username[100];
    int id_song;
} votesDB[1000];
int nrVotesDB = 0;

struct
{
    int id_song;
    char genre[100];
} genresDB[1000];
int nrGenresDB = 0;

void *Menu::treatUser(void *pointerUser)
{
    User user;
    user = *(User *) pointerUser;

    int fd = user.getSockfd();
    user.setSockfd(fd);
    user.setAddr(user.getAddr());

    printf("[Thread %d] Connection established with the client.\n", user.getSockfd());
    fflush(stdout);

    Menu::startMenu(user);

    return (NULL);
};

// FUNCTII PENTRU COMUNICARE
bool Menu::getMsg(User &user)
{
    bzero(msgReceived, 100);
    receivedLen = read(user.getSockfd(), msgReceived, sizeof(msgReceived));
    if(receivedLen < 0)
    {
        printf("[Thread %d]\n", user.getSockfd());
        throw "Error: read() from client.\n";
    }
}

void Menu::printReceived(User &user)
{
    // afisam mesajul primit
    printf("[Thread %d] The message received is: %s\n", user.getSockfd(), msgReceived);
}

void Menu::sendMsg(User &user)
{
    // returnam mesajul clientului
    if(write(user.getSockfd(), msgSent, sizeof(msgSent)) <= 0)
    {
        printf("[Thread %d] ", user.getSockfd());
        throw "[Thread] Error: write() to client.\n";
    }
}

void Menu::setSent(char *msg)
{
    bzero(msgSent, 100);
    strcat(msgSent, msg);
}

void Menu::printSent(User &user)
{
    // afisam mesajul primit
    printf("[Thread %d] The message sent is: %s\n", user.getSockfd(), msgSent);
}

// PENTRU BAZA DE DATE
static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i;
    for(i = 0; i < argc; i++)
    {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}

void *Menu::openDB()
{
    rc = sqlite3_open("music.db", &db);
    if(rc)
    {
        fprintf(stderr, "[database] Can't open database: %s\n", sqlite3_errmsg(db));
        return (0);
    } else
    {
        fprintf(stdout, "[database] Opened database successfully\n");
    }
}

static int callbackUsersDB(void *NotUsed, int argc, char **argv, char **azColName)
{
    char piece[100];
    bzero(piece, 100);

    int i = 0;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    strcpy(usersDB[nrUsersDB].username, piece);
    i++;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    strcpy(usersDB[nrUsersDB].password, piece);
    i++;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    strcpy(usersDB[nrUsersDB].admin, piece);
    i++;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    strcpy(usersDB[nrUsersDB].to_vote, piece);
    nrUsersDB++;
    return 0;
}

void Menu::getUsersDB()
{
    nrUsersDB = 0;
    const char *data = "Callback function called";
    sql = (char *) "SELECT username, password, admin, to_vote FROM users";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callbackUsersDB, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (users data) SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] Got the users data.\n");
    }
}

void Menu::insertUsersDB(User &user)
{
    const char *data = "Callback function called";
    sql = (char *) "INSERT INTO USERS (USERNAME,PASSWORD,ADMIN,TO_VOTE)" \
                   "VALUES ('";
    char newSql[200];
    bzero(newSql, 200);
    strcpy(newSql, sql);
    strcat(newSql, user.getUsername());
    strcat(newSql, "', '");
    strcat(newSql, user.getPassord());

    strcat(newSql, "', '");
    strcat(newSql, user.getAdmin());
    strcat(newSql, "', 'YES');");
    sql = newSql;

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (thread %d) SQL error: %s\n", user.getSockfd(), zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] (thread %d) User inserted.\n", user.getSockfd());
    }
}

static int callbackSongsDB(void *NotUsed, int argc, char **argv, char **azColName)
{
    char piece[100];
    bzero(piece, 100);

    int i = 0;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    songsDB[nrSongsDB].id = atoi(piece);

    i++;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    strcpy(songsDB[nrSongsDB].title, piece);

    i++;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    strcpy(songsDB[nrSongsDB].artist, piece);

    i++;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    strcpy(songsDB[nrSongsDB].link, piece);

    i++;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    strcpy(songsDB[nrSongsDB].description, piece);

    i++;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    songsDB[nrSongsDB].votes = atoi(piece);

    nrSongsDB++;
    return 0;
}

void Menu::getSongsDB()
{
    nrSongsDB = 0;

    const char *data = "Callback function called";
    sql = (char *) "SELECT id, title, artist, link, description, votes FROM songs ORDER BY votes DESC";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callbackSongsDB, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (songs data) SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] Got the songs data.\n");
    }

    for(int s = 0; s < nrSongsDB; s++)
        songsDB[s].nrGenres = 0;
    Menu::getGenresDB();
    for(int i = 0; i < nrGenresDB; i++)
        for(int s = 0; s < nrSongsDB; s++)
            if(songsDB[s].id == genresDB[i].id_song)
            {
                strcpy(songsDB[s].genres[songsDB[s].nrGenres], genresDB[i].genre);
                songsDB[s].nrGenres++;
            }
}

void Menu::insertSongDB(User &user, song &s)
{
    const char *data = "Callback function called";
    sql = (char *) "INSERT INTO SONGS (ID,TITLE,ARTIST,LINK,DESCRIPTION,VOTES)" \
                   "VALUES (";
    char newSql[200];
    bzero(newSql, 200);
    strcpy(newSql, sql);
    char number[10];
    sprintf(number, "%d", s.id);
    strcat(newSql, number);
    strcat(newSql, ", '");
    strcat(newSql, s.title);
    strcat(newSql, "', '");
    strcat(newSql, s.artist);
    strcat(newSql, "', '");
    strcat(newSql, s.link);
    strcat(newSql, "', '");
    strcat(newSql, s.description);
    strcat(newSql, "', ");
    sprintf(number, "%d", s.votes);
    strcat(newSql, number);
    strcat(newSql, ");");
    sql = newSql;

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (thread %d) SQL error: %s\n", user.getSockfd(), zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] (thread %d) Song inserted.\n", user.getSockfd());
    }
}

void Menu::voteSongDB(User &user, int rank)
{
    const char *data = "Callback function called";
    sql = (char *) "UPDATE SONGS set VOTES = VOTES + 1 where ID=";
    char newSql[200];
    bzero(newSql, 200);
    strcpy(newSql, sql);
    char number[10];
    sprintf(number, "%d", songsDB[rank].id);
    strcat(newSql, number);
    strcat(newSql, ";");
    sql = newSql;

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (thread %d) SQL error: %s\n", user.getSockfd(), zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] (thread %d) Song voted.\n", user.getSockfd());
    }
}

static int callbackVotesDB(void *NotUsed, int argc, char **argv, char **azColName)
{
    char piece[100];
    bzero(piece, 100);

    int i = 0;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    strcpy(votesDB[nrVotesDB].username, piece);
    i++;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    votesDB[nrVotesDB].id_song = atoi(piece);
    nrVotesDB++;
    return 0;
}

void Menu::getVotesDB()
{
    nrVotesDB = 0;
    const char *data = "Callback function called";
    sql = (char *) "SELECT username, id_song FROM votes";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callbackVotesDB, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (votes data) SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] Got the votes data.\n");
    }
}

void Menu::insertVoteDB(User &user, int rank)
{
    const char *data = "Callback function called";
    sql = (char *) "INSERT INTO VOTES (USERNAME,ID_SONG)" \
                   "VALUES ('";
    char newSql[200];
    bzero(newSql, 200);
    strcpy(newSql, sql);
    strcat(newSql, user.getUsername());
    strcat(newSql, "', ");
    char number[10];
    sprintf(number, "%d", songsDB[rank].id);
    strcat(newSql, number);
    strcat(newSql, ");");
    sql = newSql;

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (thread %d) SQL error: %s\n", user.getSockfd(), zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] (thread %d) Song inserted.\n", user.getSockfd());
    }
}

static int callbackGenresDB(void *NotUsed, int argc, char **argv, char **azColName)
{
    char piece[100];
    bzero(piece, 100);

    int i = 0;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    genresDB[nrGenresDB].id_song = atoi(piece);
    i++;
    sprintf(piece, "%s", argv[i] ? argv[i] : "NULL");
    strcpy(genresDB[nrGenresDB].genre, piece);
    nrGenresDB++;
    return 0;
}

void Menu::getGenresDB()
{
    nrGenresDB = 0;
    const char *data = "Callback function called";
    sql = (char *) "SELECT id_song, genre FROM genres";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callbackGenresDB, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (genres data) SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] Got the genres data.\n");
    }
}

void Menu::insertGenreDB(User &user, int rank, char genre[])
{
    const char *data = "Callback function called";
    sql = (char *) "INSERT INTO GENRES (ID_SONG, GENRE)" \
                   "VALUES (";
    char newSql[200];
    bzero(newSql, 200);
    strcpy(newSql, sql);
    char number[10];
    sprintf(number, "%d", songsDB[rank].id);
    strcat(newSql, number);
    strcat(newSql, ", '");
    strcat(newSql, genre);
    strcat(newSql, "');");
    sql = newSql;

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (thread %d) SQL error: %s\n", user.getSockfd(), zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] (thread %d) Genre inserted.\n", user.getSockfd());
    }
}

void Menu::deleteSongDB(User &user, int rank)
{
    const char *data = "Callback function called";
    sql = (char *) "DELETE from SONGS where ID=";
    char newSql[200];
    bzero(newSql, 200);
    strcpy(newSql, sql);
    char number[10];
    sprintf(number, "%d", songsDB[rank].id);
    strcat(newSql, number);
    strcat(newSql, ";");
    sql = newSql;

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (thread %d) SQL error: %s\n", user.getSockfd(), zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] (thread %d) Song deleted.\n", user.getSockfd());
    }
}

void Menu::makeAdminDB(User &user, char username[100])
{
    const char *data = "Callback function called";
    sql = (char *) "UPDATE USERS set ADMIN = 'YES' where USERNAME='";
    char newSql[200];
    bzero(newSql, 200);
    strcpy(newSql, sql);
    strcat(newSql, username);
    strcat(newSql, "';");
    sql = newSql;

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (thread %d) SQL error: %s\n", user.getSockfd(), zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] (thread %d) The user was made admin.\n", user.getSockfd());
    }
}

void Menu::blockUserVoteDB(User &user, char username[100])
{
    const char *data = "Callback function called";
    sql = (char *) "UPDATE USERS set TO_VOTE = 'NO' where USERNAME='";
    char newSql[200];
    bzero(newSql, 200);
    strcpy(newSql, sql);
    strcat(newSql, username);
    strcat(newSql, "';");
    sql = newSql;

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, (void *) data, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "[database] (thread %d) SQL error: %s\n", user.getSockfd(), zErrMsg);
        sqlite3_free(zErrMsg);
    } else
    {
        fprintf(stdout, "[database] (thread %d) The user was made admin.\n", user.getSockfd());
    }
}

// PENTRU USER
bool Menu::startMenu(User &user)
{
    Menu::openDB();
    Menu::getUsersDB();
    Menu::getSongsDB();
    while(1)
    {
        Menu::getMsg(user);
        Menu::printReceived(user);
        if(strcmp("1", msgReceived) == 0)
        {
            Menu::setSent((char *) "signIn");
            Menu::sendMsg(user);
            Menu::printSent(user);
            Menu::signIn(user);
            Menu::startTopMusic(user);
            break;
        } else if(strcmp("2", msgReceived) == 0)
        {
            Menu::setSent((char *) "signUp");
            Menu::sendMsg(user);
            Menu::printSent(user);
            Menu::signUp(user);
            Menu::insertUsersDB(user);
            Menu::startTopMusic(user);
            break;
        } else
        {
            Menu::setSent((char *) "startMenu");
            Menu::sendMsg(user);
            Menu::printSent(user);
        }
    }
}

void Menu::signIn(User &user)
{
    bool signedIn = false;
    do
    {
        Menu::getMsg(user);
        Menu::printReceived(user);
        char username[100];
        strcpy(username, msgReceived);
        Menu::getMsg(user);
        Menu::printReceived(user);
        int id = 0;
        if(Menu::matchUsername(username, id))
        {
            user.setUsername(username);
            if(Menu::matchPassword(msgReceived, id))
            {
                signedIn = true;
                user.setPassword(msgReceived);
                user.setAdmin(usersDB[id].admin);
                user.setTo_vote(usersDB[id].to_vote);
                Menu::setSent((char *) "signedIn");
                Menu::printSent(user);
                Menu::sendMsg(user);
            } else
            {
                Menu::setSent((char *) "incorrect");
                Menu::printSent(user);
                Menu::sendMsg(user);
            }
        } else
        {
            Menu::setSent((char *) "incorrect");
            Menu::printSent(user);
            Menu::sendMsg(user);
        }
    } while(!signedIn);
}

bool Menu::matchUsername(char *msg, int &id)
{
    for(int i = 0; i < nrUsersDB; i++)
        if(strcasecmp(usersDB[i].username, msg) == 0)
        {
            id = i;
            return true;
        }
    return false;
}

bool Menu::matchPassword(char *msg, int id)
{
    if(strcmp(usersDB[id].password, msg) == 0)
        return true;
    return false;
}

void Menu::signUp(User &user)
{
    bool signedUp = false;
    do
    {
        Menu::getMsg(user);
        Menu::printReceived(user);
        char username[100];
        strcpy(username, msgReceived);
        Menu::getMsg(user);
        Menu::printReceived(user);
        if(!Menu::invalidUsername())
        {
            int id = 0;
            if(!matchUsername(username, id))
            {
                user.setUsername(username);
                if(!Menu::invalidPassword())
                {
                    user.setPassword(msgReceived);
                    Menu::setSent((char *) "signedUp");
                    Menu::printSent(user);
                    Menu::sendMsg(user);
                    user.setAdmin((char *) "NO");
                    user.setTo_vote((char *) "YES");
                    signedUp = true;
                } else
                {
                    Menu::setSent((char *) "incorrectPassword");
                    Menu::sendMsg(user);
                    Menu::printSent(user);
                }
            } else
            {
                Menu::setSent((char *) "usernameUsed");
                Menu::sendMsg(user);
                Menu::printSent(user);
            }
        } else
        {
            Menu::setSent((char *) "incorrectUsername");
            Menu::sendMsg(user);
            Menu::printSent(user);
        }
    } while(!signedUp);
}

bool Menu::invalidUsername()
{
    for(int i = 0; i < strlen(msgReceived); i++)
        if((msgReceived[i] < '0' || msgReceived[i] > '9')
           && (msgReceived[i] < 'A' || msgReceived[i] > 'Z')
           && (msgReceived[i] < 'a' || msgReceived[i] > 'z')
           && (msgReceived[i] != '.' && msgReceived[i] != '_'))
            return true;
    return false;
}

bool Menu::invalidPassword()
{
    for(int i = 0; i < strlen(msgReceived); i++)
        if((msgReceived[i] < '0' || msgReceived[i] > '9')
           && (msgReceived[i] < 'A' || msgReceived[i] > 'Z')
           && (msgReceived[i] < 'a' || msgReceived[i] > 'z')
           && (msgReceived[i] != '.' && msgReceived[i] != '_'
               && msgReceived[i] != '!' && msgReceived[i] != '('
               && msgReceived[i] != ')' && msgReceived[i] != '#'
               && msgReceived[i] != '-' && msgReceived[i] != ','
               && msgReceived[i] != '~' && msgReceived[i] != '@'))
            return true;
    return false;
}

void Menu::startTopMusic(User &user)
{
    while(1)
    {
        Menu::getSongsDB();

        sprintf(msgSent, "%d", nrSongsDB);
        Menu::sendMsg(user);
        Menu::printSent(user);

        Menu::printTop(user);

        if(strcmp(user.getAdmin(), "YES") == 0)
        {
            Menu::setSent((char *) "Admin");
            Menu::printSent(user);
            Menu::sendMsg(user);
        } else
        {
            Menu::setSent((char *) "User");
            Menu::printSent(user);
            Menu::sendMsg(user);
        }

        Menu::getMsg(user);
        Menu::printReceived(user);

        if(strcmp(msgReceived, "add a song") == 0)
            Menu::addSong(user);
        else if(strcmp(msgReceived, "add a genre to a song") == 0)
            Menu::addGenreToSong(user);
        else if(strcmp(msgReceived, "vote a song") == 0)
        {
            if(strcmp(user.getTo_vote(), "YES") == 0)
            {
                Menu::setSent((char *) "You are allowed vote");
                Menu::printSent(user);
                Menu::sendMsg(user);
                Menu::voteSong(user);
            } else
            {
                Menu::setSent((char *) "Restriction");
                Menu::printSent(user);
                Menu::sendMsg(user);
            }
        } else if(strcmp(msgReceived, "delete a song") == 0)
        {
            if(strcmp(user.getAdmin(), "YES") == 0)
            {
                Menu::setSent((char *) "Admin");
                Menu::printSent(user);
                Menu::sendMsg(user);
                Menu::deleteSong(user);
            } else
            {
                Menu::setSent((char *) "A user is not allowed to delete a song.");
                Menu::printSent(user);
                Menu::sendMsg(user);
            }
        } else if(strcmp(msgReceived, "make a user admin") == 0)
        {
            if(strcmp(user.getAdmin(), "YES") == 0)
            {
                Menu::setSent((char *) "Admin");
                Menu::printSent(user);
                Menu::sendMsg(user);
                Menu::makeAdmin(user);
            } else
            {
                Menu::setSent((char *) "A user is not allowed to delete a song.");
                Menu::printSent(user);
                Menu::sendMsg(user);
            }
        } else if(strcmp(msgReceived, "block a user from voting") == 0)
        {
            if(strcmp(user.getTo_vote(), "YES") == 0)
            {
                Menu::setSent((char *) "Admin");
                Menu::printSent(user);
                Menu::sendMsg(user);
                Menu::blockUserVote(user);
            } else
            {
                Menu::setSent((char *) "A user doesn't have this option.");
                Menu::printSent(user);
                Menu::sendMsg(user);
            }
        } else if(strcmp(msgReceived, "close app") == 0)
        {
            Menu::exit(user);
            break;
        } else if(strcmp(msgReceived, "sign out") == 0)
        {
            Menu::startMenu(user);
            break;
        }
    }

}

void Menu::printTop(User &user)
{
    printf("\n[Thread %d] Sending the top to the user.\n\n", user.getSockfd());
    for(int i = 0; i < nrSongsDB; i++)
    {
        sprintf(msgSent, "%d", songsDB[i].id);
        Menu::sendMsg(user);
        Menu::printSent(user);

        Menu::setSent(songsDB[i].title);
        Menu::sendMsg(user);
        Menu::printSent(user);

        Menu::setSent(songsDB[i].artist);
        Menu::sendMsg(user);
        Menu::printSent(user);

        Menu::setSent(songsDB[i].link);
        Menu::sendMsg(user);
        Menu::printSent(user);

        Menu::setSent(songsDB[i].description);
        Menu::sendMsg(user);
        Menu::printSent(user);

        sprintf(msgSent, "%d", songsDB[i].votes);
        Menu::sendMsg(user);
        Menu::printSent(user);

        sprintf(msgSent, "%d", songsDB[i].nrGenres);
        Menu::sendMsg(user);
        Menu::printSent(user);

        for(int g = 0; g < songsDB[i].nrGenres; g++)
        {
            setSent(songsDB[i].genres[g]);
            Menu::sendMsg(user);
            Menu::printSent(user);
        }
        printf("\n");
    }
}

void Menu::addSong(User &user)
{
    song s;
    s.id = nrSongsDB + 1;
    Menu::getMsg(user);
    Menu::printReceived(user);
    strcpy(s.title, msgReceived);

    Menu::getMsg(user);
    Menu::printReceived(user);
    strcpy(s.artist, msgReceived);

    Menu::getMsg(user);
    Menu::printReceived(user);
    strcpy(s.link, msgReceived);

    Menu::getMsg(user);
    Menu::printReceived(user);
    strcpy(s.description, msgReceived);

    if(invalidText(s.title) || invalidText(s.artist)
       || invalidText(s.link) || invalidText(s.description))
    {
        Menu::setSent((char *) "invalidText");
        Menu::printSent(user);
        Menu::sendMsg(user);
    } else
    {
        Menu::insertSongDB(user, s);
        Menu::setSent((char *) "added");
        Menu::printSent(user);
        Menu::sendMsg(user);

    }

}

bool Menu::invalidText(char *msg)
{
    for(int i = 0; i < strlen(msg); i++)
        if((msg[i] < '0' || msg[i] > '9')
           && (msg[i] < 'A' || msg[i] > 'Z')
           && (msg[i] < 'a' || msg[i] > 'z')
           && msg[i] != '.' && msg[i] != '!'
           && msg[i] != '(' && msg[i] != ')'
           && msg[i] != '#' && msg[i] != '-'
           && msg[i] != ',' && msg[i] != '/'
           && msg[i] != '?' && msg[i] != '='
           && msg[i] != ' ')
            return true;
    return false;
}

void Menu::voteSong(User &user)
{
    Menu::getVotesDB();
    Menu::getMsg(user);
    Menu::printReceived(user);
    if(strcmp(msgReceived, "YES") == 0)
    {
        Menu::getMsg(user);
        Menu::printReceived(user);
        int rank = atoi(msgReceived);
        if(Menu::voteOnce(user, rank))
        {
            Menu::setSent((char *) "acceptedVote");
            Menu::sendMsg(user);
            Menu::printSent(user);
            Menu::voteSongDB(user, rank);
            Menu::insertVoteDB(user, rank);
        } else
        {
            Menu::setSent((char *) "Voted more than once");
            Menu::printSent(user);
            Menu::sendMsg(user);
        }
    }
}

bool Menu::voteOnce(User &user, int rank)
{
    for(int i = 0; i < nrVotesDB; i++)
        if(strcmp(user.getUsername(), votesDB[i].username) == 0)
            if(songsDB[rank].id == votesDB[i].id_song)
                return false;
    return true;
}

void Menu::addGenreToSong(User &user)
{
    Menu::getVotesDB();
    Menu::getMsg(user);
    Menu::printReceived(user);
    if(strcmp(msgReceived, "YES") == 0)
    {
        Menu::getGenresDB();
        Menu::getMsg(user);
        Menu::printReceived(user);
        int rank = atoi(msgReceived);

        Menu::getMsg(user);
        Menu::printReceived(user);
        char genre[100];
        strcpy(genre, msgReceived);

        if(invalidText(genre))
        {
            Menu::setSent((char *) "invalidText");
            Menu::printSent(user);
            Menu::sendMsg(user);
        } else if(Menu::genreNotUsed(rank, genre))
        {
            Menu::setSent((char *) "acceptedGenre");
            Menu::insertGenreDB(user, rank, genre);
            Menu::printSent(user);
            Menu::sendMsg(user);
        } else
        {
            Menu::setSent((char *) "Already has this genre");
            Menu::printSent(user);
            Menu::sendMsg(user);
        }
    }
}

bool Menu::genreNotUsed(int rank, char genre[])
{
    for(int i = 0; i < nrGenresDB; i++)
        if(songsDB[rank].id == genresDB[i].id_song)
            if(strcmp(genre, genresDB[i].genre) == 0)
                return false;
    return true;
}

void Menu::deleteSong(User &user)
{
    Menu::getMsg(user);
    Menu::printReceived(user);
    if(strcmp(msgReceived, "YES") == 0)
    {
        Menu::getMsg(user);
        Menu::printReceived(user);
        int rank = atoi(msgReceived);
        Menu::deleteSongDB(user, rank);
    }
}

void Menu::makeAdmin(User &user)
{
    sprintf(msgSent, "%d", nrUsersDB);
    Menu::sendMsg(user);
    Menu::printSent(user);
    Menu::printUsers(user);

    Menu::getMsg(user);
    Menu::printReceived(user);
    char username[100];
    strcpy(username, msgReceived);
    if(!availableUsername(username))
    {
        Menu::setSent((char *) "Username does not exist");
        Menu::sendMsg(user);
        Menu::printSent(user);
    } else
    {
        Menu::setSent((char *) "Username exists");
        Menu::sendMsg(user);
        Menu::printSent(user);
        Menu::getMsg(user);
        Menu::printReceived(user);
        if(strcmp(msgReceived, "YES") == 0)
            Menu::makeAdminDB(user, username);
    }
}

void Menu::printUsers(User &user)
{
    printf("\n[Thread %d] Sending the users list.\n\n", user.getSockfd());
    for(int i = 0; i < nrUsersDB; i++)
    {
        Menu::setSent(usersDB[i].username);
        Menu::sendMsg(user);
        Menu::printSent(user);
    }
}

bool Menu::availableUsername(char username[100])
{
    for(int i = 0; i < nrUsersDB; i++)
    {
        printf("\n comparing %s with %s \n", username, usersDB[i].username);
        if(strcmp(username, usersDB[i].username) == 0)
            return true;
    }
    return false;
}

void Menu::blockUserVote(User &user)
{
    sprintf(msgSent, "%d", nrUsersDB);
    Menu::sendMsg(user);
    Menu::printSent(user);
    Menu::printUsers(user);

    Menu::getMsg(user);
    Menu::printReceived(user);
    char username[100];
    strcpy(username, msgReceived);
    if(!availableUsername(username))
    {
        Menu::setSent((char *) "Username does not exist");
        Menu::sendMsg(user);
        Menu::printSent(user);
    } else
    {
        Menu::setSent((char *) "Username exists");
        Menu::sendMsg(user);
        Menu::printSent(user);
        Menu::getMsg(user);
        Menu::printReceived(user);
        if(strcmp(msgReceived, "YES") == 0)
            Menu::blockUserVoteDB(user, username);
    }
}

void Menu::exit(User &user)
{
    printf("[Thread %d] The client closed the application.\n", user.getSockfd());
    // am terminat cu acest client, inchidem conexiunea
    close(user.getSockfd());
    pthread_detach(pthread_self()); // cand se termina executia thread-ul este uitat
}