#ifndef TOPMUSIC_USER_H
#define TOPMUSIC_USER_H

#include <cstdlib>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdio>
#include <cstring>

class User
{

private:
    int sockfd;
    struct sockaddr_in addr;
    char username[100];
    char passord[100];
    char admin[5];
    char to_vote[5];

public:
    User();

    int getSockfd();
    struct sockaddr_in getAddr();

    void setSockfd(int sockfd);
    void setAddr(struct sockaddr_in address);

    char *getUsername();
    char *getPassord();
    char *getAdmin();
    char *getTo_vote();

    void setUsername(char msg[]);
    void setPassword(char msg[]);
    void setAdmin(char msg[]);
    void setTo_vote(char msg[]);
};

#endif //TOPMUSIC_USER_H
