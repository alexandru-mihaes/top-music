#ifndef TOPMUSIC_SERVER_H
#define TOPMUSIC_SERVER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#include "../Server/User.h"
#include "../Server/Menu.h"

class Server
{
private:
    int port;
    struct sockaddr_in server; // structura folosita de server
    struct sockaddr_in addr;
    int sockfd;      // descriptorul de socket

    void createSocket(int domain, int type, int protocol);
    void bindSocket();
    void acceptClient();

public:
    Server(int port);
    void start();
};

#endif //TOPMUSIC_SERVER_H
