#include "User.h"

User::User()
{}

int User::getSockfd()
{
    return this->sockfd;
}

struct sockaddr_in User::getAddr()
{
    return this->addr;
}

void User::setSockfd(int sockfd)
{
    this->sockfd = sockfd;
}

void User::setAddr(struct sockaddr_in addr)
{
    this->addr = addr;
}

char *User::getUsername()
{
    return username;
}

char *User::getPassord()
{
    return passord;
}

char *User::getAdmin()
{
    return admin;
}

char *User::getTo_vote()
{
    return to_vote;
}

void User::setUsername(char msg[])
{
    strcpy(username, msg);
}

void User::setPassword(char msg[])
{
    strcpy(passord, msg);
}

void User::setAdmin(char msg[])
{
    strcpy(admin, msg);
}

void User::setTo_vote(char msg[])
{
    strcpy(to_vote, msg);
}