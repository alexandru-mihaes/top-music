#include "Server.h"

Server::Server(int port)
{
    this->port = port;
    this->createSocket(AF_INET, SOCK_STREAM, 0);
    this->bindSocket();
    this->acceptClient();
}

void Server::createSocket(int domain, int type, int protocol)
{
    if((sockfd = socket(domain, type, protocol)) == -1)
        throw "[server] Error: socket().\n";

    int on = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)); // utilizarea optiunii SO_REUSEADDR
}

void Server::bindSocket()
{
    bzero(&server, sizeof(server)); // pregatim structurile de date
    bzero(&addr, sizeof(addr));

    // umplem structura folosita de server
    server.sin_family = AF_INET;                // stabilirea familiei de socket-uri
    server.sin_addr.s_addr = htonl(INADDR_ANY); // acceptam orice adresa
    server.sin_port = htons(port);              // utilizam un port utilizator

    if(bind(sockfd, (struct sockaddr *) &server, sizeof(struct sockaddr)) == -1) // atasam socketul
        throw "[server] Error: bind().\n";
}

void Server::acceptClient()
{
    if(listen(sockfd, 2) == -1) // punem serverul sa asculte daca vin clienti sa se conecteze
        throw "[server] Error: listen().\n";
}

void Server::start()
{
    printf("[server] Waiting at port %d...\n", port);
    while(1)
    {
        int sockfdClient;
        socklen_t length = sizeof(addr);
        if((sockfdClient = accept(sockfd, (struct sockaddr *) &addr, &length)) < 0)
            throw "[server] Error: accept().\n";

        pthread_t thread;
        User user;
        user.setSockfd(sockfdClient);
        user.setAddr(addr);

        pthread_create(&thread, NULL, Menu::treatUser, &user);
        // functia creaza un thread (post-thread)
        // &treatUser pointeaza la functia definita mai sus
        // treat ruleaza noul fir de executie
    }
}