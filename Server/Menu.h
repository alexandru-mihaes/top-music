#ifndef TOPMUSIC_MENU_H
#define TOPMUSIC_MENU_H

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <unistd.h>
#include <cerrno>
#include <cstdio>
#include <arpa/inet.h>
#include <cstring>
#include <pthread.h>
#include <sqlite3.h>

#include "../Server/User.h"

struct song
{
    int id = 0;
    char title[100];
    char artist[100];
    char link[100];
    char description[100];
    int votes = 0;
};

class Menu
{
    // FUNCTII PENTRU COMUNICARE
    static bool getMsg(User &user);
    static void printReceived(User &user);
    static void sendMsg(User &user);
    static void setSent(char *msg);
    static void printSent(User &user);

    // PENTRU BAZA DE DATE
    static void *openDB();
    static void getUsersDB();
    static void insertUsersDB(User &user);
    static void getSongsDB();
    static void insertSongDB(User &user, song &s);
    static void voteSongDB(User &user, int rank);
    static void getVotesDB();
    static void insertVoteDB(User &user, int rank);
    static void getGenresDB();
    static void insertGenreDB(User &user, int rank, char genre[]);
    static void deleteSongDB(User &user, int rank);
    static void makeAdminDB(User &user, char username[100]);
    static void blockUserVoteDB(User &user, char username[100]);

    // PENTRU USER
    static bool startMenu(User &user);
    static void signIn(User &user);
    static bool matchUsername(char *msg, int &id);
    static bool matchPassword(char *msg, int id);
    static void signUp(User &user);
    static bool invalidUsername();
    static bool invalidPassword();
    static void startTopMusic(User &user);
    static void printTop(User &user);
    static void addSong(User &user);
    static bool invalidText(char *msg);
    static void voteSong(User &user);
    static bool voteOnce(User &user, int rank);
    static void addGenreToSong(User &user);
    static bool genreNotUsed(int rank, char genre[]);
    static void deleteSong(User &user);
    static void makeAdmin(User &user);
    static void printUsers(User &user);
    static bool availableUsername(char username[100]);
    static void blockUserVote(User &user);
    static void exit(User &user);

public:
    static void *treatUser(void *arg);
};

#endif //TOPMUSIC_MENU_H
