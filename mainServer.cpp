#include "Server/Server.h"

int main()
{
    try
    {
        Server server(2908);
        server.start();
    } catch(char const *string)
    {
        perror(string);
    }
}